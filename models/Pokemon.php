<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pokemon
 *
 * @author pabhoz
 */
class Pokemon implements IModel{
    private $nombre;
    private $genero;
    private $tipo;
    private $ataques = [];
    private $nivel;
    //put your code here
       public function __construct($nombre,$genero,$tipo,$ataques,$nivel) {
        parent::__construct();
        $this->nombre = $nombre;
        $this->genero = $genero;
        $this->tipo = $tipo;
        $this->ataques = $ataques;
        $this->nivel = $nivel;
    }
    
     function getNombre() {
        return $this->nombre;
    }

    function getGenero() {
        return $this->genero;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getAtaques() {
        return $this->ataques;
    }

    function getNivel() {
        return $this->nivel;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setGenero($genero) {
        $this->genero = $genero;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setAtaques($ataques) {
        $this->ataques = $ataques;
    }

    function setNivel($nivel) {
        $this->nivel = $nivel;
    }
    
    public function getMyVars() {
        return get_object_vars($this);
    }
}
