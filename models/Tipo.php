<?php

class Tipo implements IModel{
    
    private $tipo;
    private $fortaleza = [];
    private $debilidades = [];
    
    
    public function __construct($tipo,$fortaleza,$debilidades) {
        parent::__construct();
        $this->nombre = $tipo;
        $this->tipo = $fortaleza;
        $this->dano = $debilidades;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getFortaleza() {
        return $this->fortaleza;
    }

    function getDebilidades() {
        return $this->debilidades;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setFortaleza($fortaleza) {
        $this->fortaleza = $fortaleza;
    }

    function setDebilidades($debilidades) {
        $this->debilidades = $debilidades;
    }

       
    public function getMyVars() {
        return get_object_vars($this);
    }

}
