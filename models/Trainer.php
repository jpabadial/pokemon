<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Trainer
 *
 * @author pabhoz
 */
class Trainer implements IModel{
 
    //put your code here
    private $nombre;
    private $medallas;
    private $edad;
    private $pueblo;
    private $pokemons = [];
    private $espiritu;
    
    public function __construct($nombre,$medallas,$edad,$pueblo,$pokemons,$espiritu) {
        parent::__construct();
        $this->nombre = $nombre;
        $this->medallas = $medallas;
        $this->edad = $edad;
        $this->pueblo = $pueblo;
        $this->pokemons = $pokemons;
        $this->espiritu = $espiritu;
    }
    

    function getNombre() {
        return $this->nombre;
    }

    function getMedallas() {
        return $this->medallas;
    }

    function getEdad() {
        return $this->edad;
    }

    function getPueblo() {
        return $this->pueblo;
    }

    function getPokemons() {
        return $this->pokemons;
    }

    function getEspiritu() {
        return $this->espiritu;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setMedallas($medallas) {
        $this->medallas = $medallas;
    }

    function setEdad($edad) {
        $this->edad = $edad;
    }

    function setPueblo($pueblo) {
        $this->pueblo = $pueblo;
    }

    function setPokemons($pokemons) {
        $this->pokemons = $pokemons;
    }

    function setEspiritu($espiritu) {
        $this->espiritu = $espiritu;
    }

    
    public function getMyVars() {
        return get_object_vars($this);
    }


}
