<?php

class Ataques implements IModel{
    
    private $nombre;
    private $tipo;
    private $dano;
    private $pp; //Cantidad de veces que puiede usar el ataque en el duelo.
    
    
    public function __construct($nombre,$tipo,$dano,$pp) {
        parent::__construct();
        $this->nombre = $nombre;
        $this->tipo = $tipo;
        $this->dano = $dano;
        $this->pp = $pp;
  
    }


    function getNombre() {
        return $this->nombre;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getDano() {
        return $this->dano;
    }

    function getPp() {
        return $this->pp;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setDano($dano) {
        $this->dano = $dano;
    }

    function setPp($pp) {
        $this->pp = $pp;
    }

    public function getMyVars() {
        return get_object_vars($this);
    }


}

