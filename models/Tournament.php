<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tournament
 *
 * @author pabhoz
 */
class Tournament implements IModel{
    //put your code here
    private $nombre;
    private $rondas;
    private $trainer;
    
    public function __construct($nombre,$rondas,$trainer) {
        parent::__construct();
        $this->nombre = $nombre;
        $this->tipo = $rondas;
        $this->dano = $trainer;
    }

    
    function getNombre() {
        return $this->nombre;
    }

    function getRondas() {
        return $this->rondas;
    }

    function getTrainer() {
        return $this->trainer;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setRondas($rondas) {
        $this->rondas = $rondas;
    }

    function setTrainer($trainer) {
        $this->trainer = $trainer;
    }
    

        public function getMyVars() {
        return get_object_vars($this);
    }
}
