<?php

class ModelFactory implements IModelFactory{
    
    public function newPokemon($nombre,$genero,$tipo,$ataques,$nivel): \Pokemon {
        
    }

    public function newAtaques($nombre, $medallas, $edad, $pueblo, $pokemons, $espiritu): \Ataques {
        
    }

    public function newTipo($tipo, $fortaleza, $debilidades): \Tipo {
        
    }

    public function newTrainer($nombre, $medallas, $edad, $pueblo, $pokemons, $espiritu): \Trainer {
        
    }

    public function newTournament($tipo, $fortaleza, $debilidades): \Tournament {
        
    }

}
